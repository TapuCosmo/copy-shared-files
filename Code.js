/*
  Copyright (c) 2020 Vietbao Tran (TapuCosmo)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
  OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
  OR OTHER DEALINGS IN THE SOFTWARE.
*/

// Version 1.0.0

// eslint-disable-next-line no-unused-vars
function run() {
  function copyAllShared() {
    let destinationFolder;
    if (scriptState.destinationFolderId) {
      destinationFolder = DriveApp.getFolderById(scriptState.destinationFolderId);
      activeSpreadsheet.rename(activeSpreadsheet.getName().replace("PAUSED", "RUNNING"));
    } else {
      destinationFolder = DriveApp.createFolder(`OUTPUT_${Date.now()}`);
      scriptState.destinationFolderId = destinationFolder.getId();
      activeSpreadsheet.rename(`RUNNING-Copy-Shared-Files-OUTPUT_${Date.now()}`);
    }
    const sharedFolders = utils.getSharedFolders();
    for (const sharedFolder of sharedFolders) {
      try {
        const sharedFolderId = sharedFolder.getId();
        if (scriptState.completedCopies[sharedFolderId] && scriptState.completedCopies[sharedFolderId].finished) continue;
        const sharedFolderName = sharedFolder.getName();
        const createdFolder =
          (scriptState.completedCopies[sharedFolderId] && scriptState.completedCopies[sharedFolderId].copyId) ?
            DriveApp.getFolderById(scriptState.completedCopies[sharedFolderId].copyId) :
            destinationFolder.createFolder(sharedFolderName);
        scriptState.completedCopies[sharedFolderId] = {
          copyId: createdFolder.getId(),
          finished: false
        };
        Logger.log(`Created folder ${sharedFolderName}`);
        if (utils.checkIfTimeToPause()) return "SIGINT";
        const status = utils.recursiveCopy(sharedFolder, createdFolder);
        if (status === "SIGINT") return status;
        scriptState.completedCopies[sharedFolderId].finished = true;
        Logger.log(`Finished copying folder ${sharedFolderName}`);
      } catch (e) {
        Logger.log(`Failed to copy folder ${sharedFolder}, error: ${e}`);
      }
    }
    const sharedFiles = utils.getSharedFiles();
    for (const sharedFile of sharedFiles) {
      try {
        const sharedFileId = sharedFile.getId();
        const sharedFileName = sharedFile.getName();
        if (scriptState.completedCopies[sharedFileId] && scriptState.completedCopies[sharedFileId].copyId) continue;
        const createdFile = sharedFile.makeCopy(sharedFileName, destinationFolder);
        scriptState.completedCopies[sharedFileId] = {
          copyId: createdFile.getId(),
          finished: true
        };
        Logger.log(`Copied file ${sharedFileName}`);
      } catch (e) {
        Logger.log(`Failed to copy file ${sharedFile}, error: ${e}`);
      }
      if (utils.checkIfTimeToPause()) return "SIGINT";
    }
    activeSpreadsheet.rename(activeSpreadsheet.getName().replace("RUNNING", "FINISHED"));
    activeSpreadsheet.getRange("UI!B5").setValue("FINISHED");
    utils.saveState(scriptState);
    Logger.log("Finished copying all shared folders and files.");
  }

  function copySharedFolder() {
    const sourceFolderId = utils.folderLinkToId(scriptState.sourceFolderLink);
    const sourceFolder = DriveApp.getFolderById(sourceFolderId);
    let destinationFolder;
    if (scriptState.destinationFolderId) {
      destinationFolder = DriveApp.getFolderById(scriptState.destinationFolderId);
      activeSpreadsheet.rename(activeSpreadsheet.getName().replace("PAUSED", "RUNNING"));
    } else {
      destinationFolder = DriveApp.createFolder(`${sourceFolder.getName()}_${Date.now()}`);
      scriptState.destinationFolderId = destinationFolder.getId();
      activeSpreadsheet.rename(`RUNNING-Copy-Shared-Files-${sourceFolder.getName()}_${Date.now()}`);
    }
    const status = utils.recursiveCopy(sourceFolder, destinationFolder);
    if (status === "SIGINT") return status;
    activeSpreadsheet.rename(activeSpreadsheet.getName().replace("RUNNING", "FINISHED"));
    activeSpreadsheet.getRange("UI!B5").setValue("FINISHED");
    utils.saveState(scriptState);
    Logger.log("Finished copying shared folder.");
  }

  const utils = {
    getSharedFolders() {
      const sharedFolders = DriveApp.searchFolders("sharedWithMe = true");
      return this.collectionToArray(sharedFolders);
    },
    getSharedFiles() {
      const sharedFiles = DriveApp.searchFiles("sharedWithMe = true");
      return this.collectionToArray(sharedFiles);
    },
    collectionToArray(collection) {
      const array = [];
      while (collection.hasNext()) {
        array.push(collection.next());
      }
      return array;
    },
    recursiveCopy(fromFolder, toFolder) {
      const folders = this.collectionToArray(fromFolder.getFolders());
      for (const folder of folders) {
        try {
          const folderId = folder.getId();
          if (scriptState.completedCopies[folderId] && scriptState.completedCopies[folderId].finished) continue;
          const folderName = folder.getName();
          const createdFolder =
            (scriptState.completedCopies[folderId] && scriptState.completedCopies[folderId].copyId) ?
              DriveApp.getFolderById(scriptState.completedCopies[folderId].copyId) :
              toFolder.createFolder(folderName);
          scriptState.completedCopies[folderId] = {
            copyId: createdFolder.getId(),
            finished: false
          };
          Logger.log(`Created folder ${folderName}`);
          if (this.checkIfTimeToPause()) return "SIGINT";
          const status = this.recursiveCopy(folder, createdFolder);
          if (status === "SIGINT") return status;
          scriptState.completedCopies[folderId].finished = true;
          Logger.log(`Finished copying folder ${folderName}`);
        } catch (e) {
          Logger.log(`Failed to copy folder ${folder}, error: ${e}`);
        }
      }
      const files = this.collectionToArray(fromFolder.getFiles());
      for (const file of files) {
        try {
          const fileId = file.getId();
          const fileName = file.getName();
          if (scriptState.completedCopies[fileId] && scriptState.completedCopies[fileId].copyId) continue;
          const createdFile = file.makeCopy(fileName, toFolder);
          scriptState.completedCopies[fileId] = {
            copyId: createdFile.getId(),
            finished: true
          };
          Logger.log(`Copied file ${fileName}`);
        } catch (e) {
          Logger.log(`Failed to copy file ${file}, error: ${e}`);
        }
        if (this.checkIfTimeToPause()) return "SIGINT";
      }
    },
    folderLinkToId(link) {
      const matches = link.match(/\/folders\/([^/]{16,})/);
      if (!matches || !matches[1]) {
        throw new Error("Bad link.");
      }
      return matches[1];
    },
    loadState() {
      const state = {
        sourceFolderLink: null,
        lastRowNumber: 1,
        completedCopies: {},
        destinationFolderId: null
      };
      state.sourceFolderLink = activeSpreadsheet.getRange("UI!A7").getValue() || null;
      state.lastRowNumber = activeSpreadsheet.getRange("OtherData!B1").getValue() || 1;
      if (state.lastRowNumber > 1) {
        const fileListData = activeSpreadsheet.getRange(`FileList!A2:C${state.lastRowNumber}`).getValues();
        for (const row of fileListData) {
          state.completedCopies[row[0]] = {
            copyId: row[1],
            finished: !!row[2]
          };
        }
      } else {
        state.lastRowNumber = 1;
      }
      state.destinationFolderId = activeSpreadsheet.getRange("OtherData!B2").getValue() || null;
      return state;
    },
    saveState(state) {
      const fileListArray = [];
      for (const id of Object.keys(state.completedCopies)) {
        fileListArray.push([id, state.completedCopies[id].copyId || "", state.completedCopies[id].finished]);
      }
      state.lastRowNumber = fileListArray.length + 1;
      activeSpreadsheet.getRange("OtherData!B1").setValue(state.lastRowNumber);
      activeSpreadsheet.getRange(`FileList!A2:C${state.lastRowNumber}`).setValues(fileListArray);
      activeSpreadsheet.getRange("OtherData!B2").setValue(state.destinationFolderId);
    },
    checkIfTimeToPause() {
      const currentTime = Date.now();
      const timeRunning = currentTime - startTime;
      if (timeRunning > 300000) {
        this.pause(300000 - timeRunning + 60000);
        return true;
      }
      return false;
    },
    pause(time) {
      this.saveState(scriptState);
      ScriptApp.newTrigger("run")
        .timeBased()
        .after(time)
        .create();
      activeSpreadsheet.rename(activeSpreadsheet.getName().replace("RUNNING", "PAUSED"));
      activeSpreadsheet.getRange("UI!B5").setValue("PAUSED");
      Logger.log("Pausing script.");
    }
  };

  const startTime = Date.now();
  const activeSpreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  activeSpreadsheet.getRange("UI!B5").setValue("RUNNING");
  const scriptState = utils.loadState();

  if (scriptState.sourceFolderLink) {
    const status = copySharedFolder();
    if (status === "SIGINT") return status;
  } else {
    const status = copyAllShared();
    if (status === "SIGINT") return status;
  }
}
